// console.log("Hello World!");

	const getCube = Math.pow(2, 3);
	console.log(`The cube of 2 is ${getCube}`);

	const address = ["258", "Washington Ave NW", "California 90011"];
	console.log(`I live at ${address[0]} ${address[1]}, ${address[2]}`);

	const animal = {
		name: "Lolong",
		species: "salt water crocodile",
		weight: 1075,
		measurement: "20 ft 3 in"
	}

	function getInformation({name, species, weight, measurement}){
		console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)
	}
	getInformation(animal);

	const arrayNumber = [1, 2, 3, 4 ,5];

	arrayNumber.forEach((element) => {
 	console.log(element);
	});

	const sum = arrayNumber.reduce((total, n) => total + n, 0);

	console.log(sum);

	class Dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}


	let dog = new Dog("Tiny", 2, "Mini pinscher");
	console.log(dog);